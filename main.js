// Add event listener to the form submit button
document.querySelector("form").addEventListener("submit", function (e) {
  e.preventDefault(); // Prevent form submission

  // Get the input value
  const taskInput = document.querySelector("input[type='text']");
  const taskText = taskInput.value.trim();

  if (taskText !== "") {
    // Create a new todo item
    const todoItem = document.createElement("li");
    todoItem.classList.add(
      "bg-gray-200",
      "rounded-lg",
      "shadow-md",
      "p-4",
      "flex",
      "items-center",
      "justify-between",
    );

    // Create the checkbox
    const checkbox = document.createElement("input");
    checkbox.type = "checkbox";
    checkbox.classList.add("form-checkbox", "h-5", "w-5", "text-gray-500");

    // Create the task text
    const taskSpan = document.createElement("span");
    taskSpan.classList.add("ml-2");
    taskSpan.textContent = taskText;

    // Create the delete button
    const deleteButton = document.createElement("button");
    deleteButton.classList.add("text-gray-500");
    deleteButton.innerHTML = '<i class="fas fa-trash"></i>';

    // Append elements to the todo item
    todoItem.appendChild(checkbox);
    todoItem.appendChild(taskSpan);
    todoItem.appendChild(deleteButton);

    // Append the todo item to the todo list
    const todoList = document.querySelector("ul");
    todoList.appendChild(todoItem);

    // Clear the input field
    taskInput.value = "";
  }
});
